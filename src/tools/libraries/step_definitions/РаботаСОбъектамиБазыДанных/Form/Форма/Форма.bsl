﻿//начало текста модуля

///////////////////////////////////////////////////
//Служебные функции и процедуры
///////////////////////////////////////////////////

&НаКлиенте
// контекст фреймворка Vanessa-Behavior
Перем Ванесса;
 
&НаКлиенте
// Структура, в которой хранится состояние сценария между выполнением шагов. Очищается перед выполнением каждого сценария.
Перем Контекст Экспорт;
 
&НаКлиенте
// Структура, в которой можно хранить служебные данные между запусками сценариев. Существует, пока открыта форма Vanessa-Behavior.
Перем КонтекстСохраняемый Экспорт;

&НаКлиенте
// Функция экспортирует список шагов, которые реализованы в данной внешней обработке.
Функция ПолучитьСписокТестов(КонтекстФреймворкаBDD) Экспорт
	Ванесса = КонтекстФреймворкаBDD;
	
	ВсеТесты = Новый Массив;

	//описание параметров
	//Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,Снипет,ИмяПроцедуры,ПредставлениеТеста,Транзакция,Параметр);

	Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,"ЯУдаляюВсеДокументы(Парам01)","ЯУдаляюВсеДокументы","Когда Я удаляю все документы ""Документ1""");
	Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,"ВБазеЭкземляровДокумента(Парам01,Парам02)","ВБазеЭкземляровДокумента","Тогда в базе 0 экземпляров документа ""Документ1""");

	Возврат ВсеТесты;
КонецФункции
	
&НаСервере
// Служебная функция.
Функция ПолучитьМакетСервер(ИмяМакета)
	ОбъектСервер = РеквизитФормыВЗначение("Объект");
	Возврат ОбъектСервер.ПолучитьМакет(ИмяМакета);
КонецФункции
	
&НаКлиенте
// Служебная функция для подключения библиотеки создания fixtures.
Функция ПолучитьМакетОбработки(ИмяМакета) Экспорт
	Возврат ПолучитьМакетСервер(ИмяМакета);
КонецФункции

&НаСервере
Процедура ЯУдаляюВсеДокументыСервер(ВидДокумента)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ТаблицаДокументов.Ссылка
		|ИЗ
		|	Документ." + ВидДокумента + " КАК ТаблицаДокументов";
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		
		ДокОбъект = ВыборкаДетальныеЗаписи.Ссылка.ПолучитьОбъект();
		ДокОбъект.Удалить();
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Функция ЭкземляровДокументаВБазеСервер(ВидДокумента)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	КОЛИЧЕСТВО(РАЗЛИЧНЫЕ ТаблицаДокументов.Ссылка) КАК КоличествоДокументов
		|ИЗ
		|	Документ." + ВидДокумента + " КАК ТаблицаДокументов";
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Если РезультатЗапроса.Пустой() Тогда
		
		Возврат 0;
		
	Иначе
		
		ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
		ВыборкаДетальныеЗаписи.Следующий();
		Возврат ВыборкаДетальныеЗаписи.КоличествоДокументов;
		
	КонецЕсли;
	
КонецФункции

///////////////////////////////////////////////////
//Работа со сценариями
///////////////////////////////////////////////////

&НаКлиенте
// Процедура выполняется перед началом каждого сценария
Процедура ПередНачаломСценария() Экспорт
	
КонецПроцедуры

&НаКлиенте
// Процедура выполняется перед окончанием каждого сценария
Процедура ПередОкончаниемСценария() Экспорт
	
КонецПроцедуры

///////////////////////////////////////////////////
//Реализация шагов
///////////////////////////////////////////////////

&НаКлиенте
//Когда Я удаляю все документы "Документ1"
//@ЯУдаляюВсеДокументы(Парам01)
Процедура ЯУдаляюВсеДокументы(ВидДокумента) Экспорт
	
	ЯУдаляюВсеДокументыСервер(ВидДокумента);
	
КонецПроцедуры

&НаКлиенте
//Тогда в базе 0 экземляров документа "Документ1"
//@ВБазеЭкземляровДокумента(Парам01,Парам02)
Процедура ВБазеЭкземляровДокумента(ОжидаемоеЧислоЭкземпляров, ВидДокумента) Экспорт
	
	ТекЧислоЭкземпляров = ЭкземляровДокументаВБазеСервер(ВидДокумента);
	
	Если ТекЧислоЭкземпляров <> ОжидаемоеЧислоЭкземпляров Тогда
		
		ВызватьИсключение "Текущее количество документов""" + ВидДокумента + """: " + Строка(ТекЧислоЭкземпляров) + ", ожидаемое количество: " + ОжидаемоеЧислоЭкземпляров;
		
	КонецЕсли;
	
КонецПроцедуры

//окончание текста модуля