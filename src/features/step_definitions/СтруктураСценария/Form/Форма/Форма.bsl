﻿//начало текста модуля

///////////////////////////////////////////////////
//Служебные функции и процедуры
///////////////////////////////////////////////////

&НаКлиенте
// контекст фреймворка Vanessa-Behavior
Перем Ванесса;

&НаКлиенте
// Структура, в которой хранится состояние сценария между выполнением шагов. Очищается перед выполнением каждого сценария.
Перем Контекст Экспорт;
 
&НаКлиенте
// Структура, в которой можно хранить служебные данные между запусками сценариев. Существует, пока открыта форма Vanessa-Behavior.
Перем КонтекстСохраняемый Экспорт;

&НаКлиенте
Перем Слагаемое_1, Слагаемое_2;

&НаКлиенте
// Функция экспортирует список шагов, которые реализованы в данной внешней обработке.
Функция ПолучитьСписокТестов(КонтекстФреймворкаBDD) Экспорт
	Ванесса = КонтекстФреймворкаBDD;
	
	ВсеТесты = Новый Массив;

	//описание параметров
	//Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,Снипет,ИмяПроцедуры,ПредставлениеТеста,Транзакция,Параметр);

	Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,"Есть(слагаемое1)","Есть","Дано есть <слагаемое1>");
	Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,"ЯПрибавляю(слагаемое2)","ЯПрибавляю","Когда я прибавляю <слагаемое2>");
	Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,"УМеняДолжноПолучться(сумма)","УМеняДолжноПолучться","Тогда у меня должно получться <сумма>");

	Возврат ВсеТесты;
КонецФункции
	
&НаСервере
// Служебная функция.
Функция ПолучитьМакетСервер(ИмяМакета)
	ОбъектСервер = РеквизитФормыВЗначение("Объект");
	Возврат ОбъектСервер.ПолучитьМакет(ИмяМакета);
КонецФункции
	
&НаКлиенте
// Служебная функция для подключения библиотеки создания fixtures.
Функция ПолучитьМакетОбработки(ИмяМакета) Экспорт
	Возврат ПолучитьМакетСервер(ИмяМакета);
КонецФункции



///////////////////////////////////////////////////
//Работа со сценариями
///////////////////////////////////////////////////

&НаКлиенте
// Процедура выполняется перед началом каждого сценария
Процедура ПередНачаломСценария() Экспорт
	
КонецПроцедуры

&НаКлиенте
// Процедура выполняется перед окончанием каждого сценария
Процедура ПередОкончаниемСценария() Экспорт
	
КонецПроцедуры



///////////////////////////////////////////////////
//Реализация шагов
///////////////////////////////////////////////////

&НаКлиенте
//Дано есть <слагаемое1>
//@Есть(слагаемое1)
Процедура Есть(слагаемое1) Экспорт
	Слагаемое_1 = слагаемое1;
КонецПроцедуры

&НаКлиенте
//Когда я прибавляю <слагаемое2>
//@ЯПрибавляю(слагаемое2)
Процедура ЯПрибавляю(слагаемое2) Экспорт
	Слагаемое_2 = слагаемое2;
КонецПроцедуры

&НаКлиенте
//Тогда у меня должно получться <сумма>
//@УМеняДолжноПолучться(сумма)
Процедура УМеняДолжноПолучться(сумма) Экспорт
	
	ЭталоннаяСумма = Слагаемое_1 + Слагаемое_2;
	
	Если сумма <> ЭталоннаяСумма Тогда
		ВызватьИсключение НСтр("ru = 'Неверная сумма: '") + Строка(сумма) + НСтр("ru = '. Ожидалось: '") + Строка(ЭталоннаяСумма);
	КонецЕсли;
	
КонецПроцедуры

//окончание текста модуля