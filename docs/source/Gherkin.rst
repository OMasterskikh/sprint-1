Gherkin
=======

**Gherkin** - популярный язык для написания спецификаций. Требования, относящиеся к конкретному свойству системы, выносятся в отдельный файл, называемый файлом свойства (feature-файлом). Файлы свойства на языке Gherkin имеют расширение ``feature``. 

Структура файла свойства
------------------------ 

Объявление
~~~~~~~~~~

Описание требования на языке Gherkin начинается с задания отправных параметров - наименования свойства, указания цели (для чего нужен новый функционал), действующего лица (пользователь, который будет работать с функционалом) и пользы (удобства, которые принесет функционал). 

В общем случае шапка файла свойства выглядит так::

     Функция: Емкое и короткое описание свойства системы
        Для того, чтобы достичь определенных целей
        В качестве действующего лица 
        Я хочу получить определенную пользу

А в частном так::

    Функция: Удаление неиспользуемым договоров
        Для того, чтобы не хранить неиспользуемые договоры контрагентов
        В качестве маркетолога
        Я хочу сервис удобного удаления договоров, по которым не было сделок

При этом обязательно только указание названия свойства - оно задается после ключевого слова ``Функция``.

.. tip:: Вместо ключа ``Функция`` вы можете использовать любой из синонимов: ``Функциональность; Функционал; Свойство``. Синонимы для других ключей смотрите в разделе `Ключевые слова Gherkin`_

Остальные три строки (цель, пользователь, польза) не обрабатываются системами автоматизированного тестирования, но несут важную информацию для понимания целесообразности доработки - они задают контекст использования нового свойства системы.

Сценарии
~~~~~~~~

Вслед за шапкой язык Gherkin обязывает нас задать список сценариев. Задача сценариев - раскрыть разные стороны поведения свойства системы. (Помним, что один файл свойства = одно новое свойство системы, а один сценарий = одна сторона поведения свойства). Каждый сценарий начинается с ключа ``Сценарий`` и состоит из шагов. Шаг - отдельная не пустая строка внутри сценария, начинающаяся с ключевого слова (обычно используются ключи ``Когда; Тогда; И``).

Дополним наш общий feature-файл простым сценарием::

     Функция: Емкое и короткое описание свойства системы
        Для того, чтобы достичь определенных целей
        В качестве действующего лица 
        Я хочу получить определенную пользу
    Сценарий: Некая бизнес-ситуация
        Когда я выполняю первое действие
        И выполняю второе действие
        И выполняю третье действие
        Тогда я получаю некий результат
        И еще один результат

Также добавим сценарий и в файл свойства "Удаление договоров"::

    Функция: Удаление неиспользуемым договоров
        Для того, чтобы не хранить неиспользуемые договоры контрагентов
        В качестве маркетолога
        Я хочу сервис удобного удаления договоров, по которым не было сделок
    Сценарий: Работа с обработкой удаления договоров
        Когда в базе есть договор 'Договор с ООО "Пуговка"'
        И по договору 'Договор с ООО "Пуговка"' нет сделок
        И я запускаю обработку "Удаление договоров"
        Тогда в базе нет договора 'Договор с ООО "Пуговка"'

Предыстория
~~~~~~~~~~~

Предыстория - раздел файла свойства, начинается с ключевого слова ``Предыстория`` или ``Контекст``, располагается после объявления и перед списком сценариев, предназначен для задания начальных условий. Предыстория содержит шаги, которые будут выполнены перед каждым сценарием из файла. Часто используется для подготовки и проверки наличия исходных данных.

Изменим наши файлы свойств. Для общего файла::

     Функция: Емкое и короткое описание свойства системы
        Для того, чтобы достичь определенных целей
        В качестве действующего лица 
        Я хочу получить определенную пользу
    Предыстория:
        Дано необходимые исходные данные 
    Сценарий: Некая бизнес-ситуация
        Когда я выполняю первое действие
        И выполняю второе действие
        И выполняю третье действие
        Тогда я получаю некий результат
        И еще один результат

Для частного файла::
 
    Функция: Удаление неиспользуемым договоров
        Для того, чтобы не хранить неиспользуемые договоры контрагентов
        В качестве маркетолога
        Я хочу сервис удобного удаления договоров, по которым не было сделок
    Контекст:
        Дано запущен клиент тестирования
        И пользователю доступна роль "Добавление и удаление договоров"
    Сценарий: Работа с обработкой удаления договоров
        Когда в базе есть договор 'Договор с ООО "Пуговка"'
        И по договору 'Договор с ООО "Пуговка"' нет сделок
        Когда я запускаю обработку "Удаление договоров"
        Тогда в базе нет договора 'Договор с ООО "Пуговка"'

Структура сценария
~~~~~~~~~~~~~~~~~~

Нередко бывают задачи, в рамках которых необходимо выполнить несколько однотипных действий. В таком случае можно написать отдельный сценарий на каждое действие, например::

    Функция: Ручное добавление товара
        Для того, чтобы вести список товаров
        В качестве кладовщика
        Я хочу заносить в базу новый товар 
    Контекст:
        Дано запущен клиент тестирования
    Сценарий: Создание товара "Лампа настольная" цвет "зеленый" артикул "12"
        Когда я открывают справочник "Товары"
        И нажимаю на кнопку "Создать"
        И в поле "Наименование" я ввожу "Лампа настольная"
        И в поле "Цвет" я ввожу "Зеленый"
        И в поле "Артикул" я ввожу "12"  
        И нажимаю на кнопку "Записать и закрыть"
        Тогда в списке товаров есть "Лампа настольная" цвет "зеленый" артикул  "12"  
    Сценарий: Создание товара "Блокнот" цвет "" артикул "14"
        Когда я открывают справочник "Товары"
        И нажимаю на кнопку "Создать"
        И в поле "Наименование" я ввожу "Блокнот"
        И в поле "Цвет" я ввожу "в ассортименте"
        И в поле "Артикул" я ввожу "14"  
        И нажимаю на кнопку "Записать и закрыть"
        Тогда в списке товаров есть "Блокнот" цвет "в ассортименте" артикул  "14"
 
Однако описание такой функции можно сделать более красиво и лаконичным, если создать шаблон сценария и подставлять в него нужные наборы данных. На языке Gherkin создание шаблона производится с помощью ключа ``Структура сценария``, при этом параметры задаются в угловых скобках, а наборы значений этих параметров приводятся в таблице после ключевого слова ``Примеры``::

    Функция: Ручное добавление товара
        Для того, чтобы вести список товаров
        В качестве кладовщика
        Я хочу заносить в базу новый товар 
    Контекст:
        Дано запущен клиент тестирования
    Сценарий: Создание товара
        Когда я открывают справочник "Товары"
        И нажимаю на кнопку "Создать"
        И в поле "Наименование" я ввожу <Наименование>
        И в поле "Цвет" я ввожу <Цвет>
        И в поле "Артикул" я ввожу <Артикул>  
        И нажимаю на кнопку "Записать и закрыть"
        Тогда в списке товаров есть <Наименование> цвет <Цвет> артикул  <Артикул>  
        Примеры:
            | Наименование     | Цвет           | Артикул |
            | Лампа настольная | зеленый        | 12      |
            | Блокнот          | в ассортименте | 14      |

Шаги
~~~~

В языке Gherkin шаги внутри сценария условно можно разделить на три последовательных блока: Данные, Действия и Результаты. 

**Данные** - шаги, начинающие с ключа ``Дано``, предназначены для приведения системы в состояние, необходимое для правильного выполнения сценариев. В шагах ``Дано`` происходит подготовка к действиям пользователя, например::

    Дано база данных пустая

Или::

    Дано на складе "Основной" нет остатков 
    Дано есть остатки по складу "Магазин" 

**Действия** - шаги, начинающие с ключа ``Когда``, предназначены для описания операций, которые выполняются пользователем и должны привести к результату, объявленному в файле свойства::

    Когда я открываю регистр сведений "Курсы валют"
    Когда я нажимаю на кнопку "Обновить курсы" 

Или::

    Когда я открываю карточку сотрудника "Лисичкина Ирина"
    Когда я нажимаю на кнопку "Загрузить фото"
    Когда я выбираю файл "D:\Фото сотрудников\Лисичкина_И"

**Результаты** - шаги, начинающие с ключа ``Тогда``, предназначены для наблюдения конкретной практической пользы (ожидаемый результат выполнения сценария)::

    Тогда произошел обмен с узлом "Головная организация" 

Или::

    Тогда табличный документ формы с именем "Результат" стал равен:
        | 'Номенклатура' | 'Продано' | 'Количество' | 'Себестоимость' | 'Продажа' | 'Прибыль' |
        | 'Ручка 1'      | ''        | '7,000'      | '17,00'         | '70,00'   | '53,00'   |
        | ''             | 'Ручка 1' | '2,000'      | '2,00'          | '20,00'   | '18,00'   |
        | ''             | 'Ручка 3' | '5,000'      | '15,00'         | '50,00'   | '35,00'   |
        | 'Ручка 3'      | ''        | '8,000'      | '21,00'         | '88,00'   | '67,00'   |
        | ''             | 'Ручка 2' | '3,000'      | '6,00'          | '33,00'   | '27,00'   |
        | ''             | 'Ручка 3' | '5,000'      | '15,00'         | '55,00'   | '40,00'   |
        | 'Итого'        | ''        | '15,000'     | '38,00'         | '158,00'  | '120,00'  |

**Предлоги** -  шаги, начинающие с ключа ``И`` или ``Но``. Смысловой нагрузки не несут, но позволяют сделать текст сценария более понятным. Рекомендованы к использованию в случае идущих подряд шагов одного вида. Сравним - сценарий без предлогов::

    Сценарий:
        Дано в базе есть документ "Реализация товаров №1"
        Дано у покупателя "ЗАО Оптовичок" по договору "Основной" переплата "500" 
        Когда я открываю документ "Реализация товаров №1"
        Когда я перехожу на закладку "Дополнительно"
        Когда я ставлю флаг "Зачитывать аванс"
        Когда я нажимаю на кнопку "Провести и закрыть"
        Тогда я формирую отчет "Расчеты с покупателями" по покупателю "ЗАО Оптовичок"
        Тогда табличный документ формы с именем "Результат" стал равен:
        | 'Контрагент'    | 'Договор'  | 'Сумма' | 
        | 'ЗАО Оптовичок' | ''         | '0'     | 
        | ''              | 'Основной' | '0'     |
        | 'Итого'         | ''         | '0'     | 

С предлогами::

    Сценарий:
        Дано в базе есть документ "Реализация товаров №1"
        И у покупателя "ЗАО Оптовичок" по договору "Основной" переплата "500" 
        Когда я открываю документ "Реализация товаров №1"
        И я перехожу на закладку "Дополнительно"
        И я ставлю флаг "Зачитывать аванс"
        И я нажимаю на кнопку "Провести и закрыть"
        Тогда я формирую отчет "Расчеты с покупателями" по покупателю "ЗАО Оптовичок"
        И табличный документ формы с именем "Результат" стал равен:
        | 'Контрагент'    | 'Договор'  | 'Сумма' | 
        | 'ЗАО Оптовичок' | ''         | '0'     | 
        | ''              | 'Основной' | '0'     |
        | 'Итого'         | ''         | '0'     |

Ключевые слова Gherkin
-----------------------

.. table:: 

	+------------------+----------------------+
	| Ключ             | Синонимы             |
	+==================+======================+
	|                  | - И                  |
	| And              | - К тому же          |
	|                  | - Так же             |
	+------------------+----------------------+
	|                  | - Предыстория        |
	| Background       | - Контекст           |
	+------------------+----------------------+
	|                  | - Но                 |
	| But              | - А                  |
	+------------------+----------------------+
	| Examples         | - Примеры            |
	+------------------+----------------------+
	|                  | - Функция            |
	|                  | - Функциональность   |
	| Feature          | - Функционал         |
	|                  | - Свойство           |
	+------------------+----------------------+
	|                  | - Допустим           |
	| Given            | - Дано               |
	|                  | - Пусть              |
	+------------------+----------------------+
	|  Scenario        | - Сценарий           |
	+------------------+----------------------+
	|  ScenarioOutline | - Структура сценария |
	+------------------+----------------------+
	|                  | - То                 |
	| Then             | - Затем              |
	|                  | - Тогда              |
	+------------------+----------------------+
	|                  | - Если               |
	| When             | - Когда              |
	+------------------+----------------------+