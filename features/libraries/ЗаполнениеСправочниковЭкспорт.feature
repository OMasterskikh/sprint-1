﻿# encoding: utf-8
# language: ru

@ExportScenarios
@Libraries

Функционал: ЗаполнениеСправочниковЭкспорт
	Как Пользователь
	Хочу создавать элементы справочников
	Чтобы заполнять документы произвольными данными

Сценарий: Я создаю номенклатуру
	Дано Я удаляю все элементы Справочника "Номенклатура" 
	Когда Я нажимаю кнопку командного интерфейса "Номенклатура"
	И открылось окно "Номенклатура"
	И я нажимаю на кнопку "Создать группу"
	И открылось окно "Номенклатура (создание группы)"
	И в поле "Наименование" я ввожу текст "Ручки"
	Тогда я нажимаю на кнопку "Записать и закрыть"
	И открылось окно "Номенклатура"
	И я нажимаю на кнопку "Создать"
	И открылось окно "Номенклатура (создание)"
	И в поле "Наименование" я ввожу текст "Ручка 1"
	И я выбираю значение реквизита "Родитель" из формы списка
	И открылось окно "Номенклатура"
	И В форме "Номенклатура" в таблице "Список" я перехожу к строке:
	| 'Наименование' |
	| 'Ручки'        |
	И я нажимаю на кнопку "Выбрать"
	И открылось окно "Номенклатура (создание) *"
	И в поле "Артикул" я ввожу текст "25"
	Тогда я нажимаю на кнопку "Записать и закрыть"
	И открылось окно "Номенклатура"
	И я нажимаю на кнопку "Создать"
	И открылось окно "Номенклатура (создание)"
	И в поле "Наименование" я ввожу текст "Ручка 2"
	И в поле "Артикул" я ввожу текст "22"
	Тогда я нажимаю на кнопку "Записать и закрыть"
	И открылось окно "Номенклатура"
	И я нажимаю на кнопку "Создать"
	И открылось окно "Номенклатура (создание)"
	И в поле "Наименование" я ввожу текст "Ручка 3"
	И в поле "Артикул" я ввожу текст "21"
	Тогда я нажимаю на кнопку "Записать и закрыть"
	И открылось окно "Номенклатура"
	И Я закрываю окно "Номенклатура"

Сценарий: Я заполняю аналоги номенклатуры
	Дано я удаляю все записи РегистрСведений "АналогиНоменклатуры" 
	Когда Я нажимаю кнопку командного интерфейса "Аналоги номенклатуры"
	И открылось окно "Аналоги номенклатуры"
	Тогда Я создаю аналог "Ручка 1" код "2" для товара "Ручка 2" код "3" из папки "Ручки" код "1"
	Тогда Я создаю аналог "Ручка 1" код "2" для товара "Ручка 3" код "4" из папки "Ручки" код "1"
	Тогда Я создаю аналог "Ручка 2" код "3" для товара "Ручка 1" код "2" из папки "Ручки" код "1"
	Тогда Я создаю аналог "Ручка 2" код "3" для товара "Ручка 3" код "4" из папки "Ручки" код "1"
	Тогда Я создаю аналог "Ручка 3" код "4" для товара "Ручка 1" код "2" из папки "Ручки" код "1"
	Тогда Я создаю аналог "Ручка 3" код "4" для товара "Ручка 2" код "3" из папки "Ручки" код "1"
	И открылось окно "Аналоги номенклатуры"
	И Я закрываю окно "Аналоги номенклатуры"

Сценарий: Я создаю склад
	Дано Я удаляю все элементы Справочника "Склады"
	Когда Я нажимаю кнопку командного интерфейса "Склады"
	И открылось окно "Склады"
	И я нажимаю на кнопку "Создать"
	И открылось окно "Склад (создание)"
	И в поле "Наименование" я ввожу текст "Основной"
	Тогда я нажимаю на кнопку "Записать и закрыть"
	И открылось окно "Склады"
	И Я закрываю окно "Склады"